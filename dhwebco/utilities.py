
"""
Generate cropped thumbnails
"""
def generate_thumbnails(path, max_width, max_height, force=True):
    from PIL import Image, ImageOps

    img = Image.open(path)
    if img.mode != 'RGBA':
            img = img.convert('RGBA')

    img = ImageOps.fit(img, (max_width, max_height,), method=Image.ANTIALIAS)

    orig_path, _, orig_extension = path.rpartition('.')
    postfix = "-thumb"
    thumb_path = "%s%s.%s" % (orig_path, postfix, orig_extension)

    if img.format == "JPEG" and "THUMBNAIL_JPEG_QUALITY" in dir(settings):
        img.save(thumb_path, quality = 80, optimize = True)
    else:
        img.save(thumb_path)