import os
from django import template
from django.conf import settings

register = template.Library()

@register.filter(name='gallery')
def gallery(value):
	result = '<ul class="gallery clearfix">'
	path = os.path.join(settings.MEDIA_DIR, 'img', 'work', value)

	try:
		for img in os.listdir(path):
			url = settings.SITE_WWW_URL + '/media/img/work/' + value + '/'
			fileName, fileExtension = os.path.splitext(img)
			if fileExtension == '.jpg' or fileExtension == '.png' or fileExtension == '.gif':
				result += '<li>'
				result += '<a href="' + url + img + '" rel="lightbox[' + value + ']">'
				result += '<img src="' + url + fileName + '-thumb' + fileExtension + '" />'
				result += '</a>'
				result += '</li>'

		result += '</ul>'
		return result
	except:
		return ''
