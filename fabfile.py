from fabric.api import *
import os
import fabric.contrib.project as project
from dhwebco import utilities

deploy_path = './deploy'

def clean():
	local('rm -rf ' + deploy_path)

def generate():
	local('hyde -g')
	genthumbs()

def regen():
	clean()
	generate()

def serve():
	local('hyde -w -k')

def reserve():
	regen()
	serve()

def smush():
	local('smusher ./media/img')

def genthumbs():
	workDir = os.path.join(deploy_path, 'media', 'img', 'work')
	for folder in os.listdir(workDir):
		for img in os.listdir(os.path.join(workDir, folder)):
			utilities.generate_thumbnails(os.path.join(workDir, folder, img), 110, 110)